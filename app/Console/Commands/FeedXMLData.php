<?php

namespace App\Console\Commands;

use App\DataFeeded;
use Illuminate\Console\Command;
use SimpleXMLElement;

class FeedXMLData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:FeedXMLData{url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This commmand use to feed xml file data via url';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = $this->argument('url');
        $xmlData = file_get_contents($url);
        $xml = new SimpleXMLElement($xmlData);
        
        //count data feeded 
        $i = 1;
        $bar = $this->output->createProgressBar(count($xml->channel->item));

        foreach($xml->channel->item as $ite){
            // $this->performTask($ite);
            $this->info('Display this XML DATA [title]: '.$ite->title);
            
            $feedxmldata = new DataFeeded;
            $feedxmldata->title = $ite->title;
            $feedxmldata->description = $ite->description;
            $feedxmldata->link = $ite->link;
            $feedxmldata->category = $ite->category;
            $feedxmldata->comments = $ite->comments;
            $feedxmldata->pubDate = $ite->pubDate;
            if ($feedxmldata->save()) {
                $this->info('->Feeded: '.$i.' item');
            }
            $i++; 

            $bar->advance();
            
        }
        $bar->finish(); 
    }
}
/* run this command on console cmd/ terminal
 php artisan feed:feedXMLData "https://www.feedforall.com/sample.xml" */