<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index</title>
    <script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.2.js"></script>
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css" />
</head>

<body>
    <h1>Hello</h1>
    <div class="container" style="padding:10px 10px;">

        <h2>Feeded XML Data</h2>
        <div class="well clearfix">
            <!-- <fieldset class="scheduler-border"> -->
            <h4 class="scheduler-border">Add Item</h4>
            <form method="POST" action="{{Route('store')}}" class="form-inline">
                @csrf
                <div class="form-group">
                    <label for="exampleInputName">Title</label>
                    <input type="text" class="form-control" id="title" name="title">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputName">Description</label>
                    <input type="text" class="form-control" id="description" name="description" value="">
                    @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputName">Link</label>
                    <input type="text" class="form-control" id="link" name="link" value="">
                    @error('link')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputName">Category</label>
                    <input type="text" class="form-control" id="category" name="category" value="">
                    @error('category')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputName">Comments</label>
                    <input type="text" class="form-control" id="comments" name="comments" value="">
                    @error('comments')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputName">Publish Date</label>
                    <input type="text" class="form-control" id="pubDate" name="pubDate" value="">
                    @error('pubDate')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Row</button>
            </form>
            <!-- </fieldset> -->
        </div>
        <table class="table table-bordered" id="tbl_employees">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Link</th>
                    <th>Category</th>
                    <th>Comments</th>
                    <th>Publish Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="tbl_posts_body">
                @foreach($dataFeededRes as $data)
                <tr>
                    <<td>{{$data->id}}</td>
                        <td>{{$data->title}}</td>
                        <td>{{$data->description}}</td>
                        <td>{{$data->link}}</td>
                        <td>{{$data->category}}</td>
                        <td>{{$data->comments}}</td>
                        <td>{{$data->pubDate}}</td>
                        <td>
                            <a class="btn btn-xs delete-record" href="{{Route('delete', $data->id)}}">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                            <a class="btn btn-xs delete-record" href="{{Route('edit', $data->id)}}">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="container">
        {{$dataFeededRes->links()}}
        </div>
    </div>
</body>

</html>